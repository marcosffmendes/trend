@extends('layouts.app')

@section('content')
  <div class="box">
    <div class="columns is-centered">
      <div class="column is-3">
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                <div class="field">
                  <label class="label">Email</label>
                  <div class="control has-icons-left">
                    <input class="input" type="email" placeholder="Email input" value="{{ old('email') }}" name="email" required>
                    <span class="icon is-small is-left">
                      <i class="fas fa-envelope"></i>
                    </span>
                    @if ($errors->has('email'))
                        <span>
                            <strong class="has-text-danger">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
            <div class="field">
            <label class="label">Password</label>
              <p class="control has-icons-left">
                <input class="input" type="password" placeholder="Password" name="password" required>
                <span class="icon is-small is-left">
                  <i class="fas fa-lock"></i>
                </span>
              </p>
              @if ($errors->has('password'))
                  <span>
                      <strong class="has-text-danger">{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="field has-text-centered">
              <input class="is-checkradio is-circle" id="lembrar" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="lembrar" class="label">Lembrar-me</label>
            </div>
            <div class="field has-text-centered">
              <button type="submit" class="button is-primary is-rounded">
                <span class="icon">
                  <i class="fas fa-sign-in-alt"></i>
                </span>
                <span>Fazer login</span> 
              </button>
            </div>
        </form>
       </div> 
    </div>
  </div>  
@endsection
