@extends('layouts.app')

@section('content')
  <div class=box>
    <div class="columns is-centered">
        <div class="column is-3">
          <form class="form-horizontal" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
                  <div class="field">
                    <label class="label">Email</label>
                    <div class="control has-icons-left">
                      <input class="input" type="email" placeholder="Endereço email" name="email" value="{{ old('email') }}" required>
                      <span class="icon is-small is-left">
                        <i class="fas fa-envelope"></i>
                      </span>
                      @if ($errors->has('email'))
                          <span>
                              <strong class="has-text-danger">{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
              <div class="field">    
                <label class="label">Número sócio</label>
                <div class="control has-icons-left">
                    <input type="number" class="input" placeholder="Número sócio" name="nsocio" value="{{ old('nsocio') }}" required>
                      <span class="icon is-small is-left">
                      <i class="fas fa-list-ol"></i>
                      </span>
                    @if ($errors->has('nsocio'))
                          <span>
                              <strong class="has-text-danger">{{ $errors->first('nsocio') }}</strong>
                          </span>
                      @endif
                </div>
              </div>
              <div class="field">
              <label class="label">Password da app da trend</label>
                <div class="control has-icons-left">
                  <input class="input" type="password" placeholder="Password da App da Trend" name="passwordApp" required>
                  <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                  </span>
                </div>
                @if ($errors->has('passwordApp'))
                    <span>
                        <strong class="has-text-danger">{{ $errors->first('passwordApp') }}</strong>
                    </span>
                @endif
              </div>
              <div class="field">
              <label class="label">Password (pode ser a mesma da app da trend)</label>
                <div class="control has-icons-left">
                  <input class="input" type="password" placeholder="Password" name="password" required>
                  <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                  </span>
                </div>
                @if ($errors->has('password'))
                    <span>
                        <strong class="has-text-danger">{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
              <div class="field">
                    <label class="label">Confirmar Password</label>

                    <div class="control has-icons-left">
                        <input id="password-confirm" type="password" placeholder="Confirmar password" class="input" name="password_confirmation" required>
                        <span class="icon is-small is-left">
                          <i class="fas fa-lock"></i>
                        </span>
                    </div>
              </div>
              <div class="field has-text-centered">
                <button type="submit" class="button is-primary is-rounded">
                <span class="icon">
                    <i class="fas fa-plus"></i>
                    </span>
                    <span>Registrar</span> 
                </button>
              </div>
          </form>
        </div> 
    </div>
  </div>
@endsection
