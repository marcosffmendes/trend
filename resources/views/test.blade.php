@extends('layouts.app')

@section('content')
      <section class="section">
        <div class="container has-text-centered">
          <h1 class="title">
            Test page {{$msg}}
          </h1>  
        </div>
      </section>
@endsection