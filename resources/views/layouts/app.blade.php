<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <link rel="stylesheet" href="css/bulma-extensions.min.css">
    <link rel="shortcut icon" type="image/ico" href="favicon.ico"/>
    <link rel="manifest" href="manifest.json">

</head>
<body>
  <nav class="navbar">
    <div class="container">
      <div class="navbar-brand">
        <span class="navbar-item">
          <a class="button is-primary is-rounded" href="{{ url('/') }}">
            <span class="icon">
              <i class="fas fa-home"></i>
            </span>
            <span>Início</span>  
          </a>
        </span>
        <span class="navbar-burger burger" data-target="navbarMenuHeroA">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </span>
      </div>
      <div id="navbarMenuHeroA" class="navbar-menu">
        <div class="navbar-end">
        @guest
            <span class="navbar-item is-active"><a class="button is-primary is-rounded" href="{{ route('login') }}">
            <span class="icon">
                  <i class="fas fa-sign-in-alt"></i>
                </span>
                <span>Fazer login</span></a></span>
            <span class="navbar-item"><a class="button is-primary is-rounded" href="{{ route('register') }}">
            <span class="icon">
                    <i class="fas fa-plus"></i>
                    </span>
                    <span>Registrar</span></a></span>
        @else
          <span class="navbar-item is-active">
          Olá {{Auth::user()->email}}
          </span>
          <span class="navbar-item is-active">
            <a class="button is-primary is-rounded" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
              <span class="icon">
              <i class="fas fa-sign-out-alt"></i>
              </span>
              <span>Sair</span> 
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </span>
        @endif   
        </div>
      </div> 
    </div>
  </nav>

  @yield('content')
 
  <script>
  // Bulma NavBar Burger Script
  document.addEventListener('DOMContentLoaded', function () {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {
                
                // Get the target from the "data-target" attribute
                let target = $el.dataset.target;
                let $target = document.getElementById(target);
                
                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
                
            });
        });
    }
  });

  function closeNotif() {
    var x = document.getElementById("notif");
      x.style.display = "none";
  } 

  if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('sw.js', { scope: '/trend/' }).then(function(reg) {

    if(reg.installing) {
      console.log('Service worker installing');
    } else if(reg.waiting) {
      console.log('Service worker installed');
    } else if(reg.active) {
      console.log('Service worker active');
    }

  }).catch(function(error) {
    // registration failed
    console.log('Registration failed with ' + error);
  });
}

  </script>
</body>
</html>