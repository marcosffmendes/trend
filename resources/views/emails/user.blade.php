@component('mail::message')
<p style="text-align: center; font-size: 1.2rem; font-weight: bold"> Olá {{ $user->nickname }} ({{ $user->email }})</p>

<p style="text-align: center;"> A marcação para {{ $data }} foi efetuada com sucesso! </p>

<p style="text-align: center;"> Caso não vás, não te esqueças de desmarcar a aula :) </p>

@component('mail::button', ['url' => $linkGoogle])
Adiciona ao calendário Google
@endcomponent

*****

<p style="text-align: center;">Para adicionar a marcação ao calendário iOS ou Outlook, basta correr o ficheiro em anexo.</p>

*****

<p style="text-align: center;"> Obrigado, </p>
<p style="text-align: center;"> <a href="https://marcosmendes.net:8443/trend">{{ config('app.name') }} </a></p>
@endcomponent
