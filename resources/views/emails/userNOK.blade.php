@component('mail::message')
<p style="text-align: center; font-size: 1.2rem; font-weight: bold"> Olá {{ $user->nickname }} ({{ $user->email }})</p>

<p style="text-align: center;"> A marcação para {{ $data }} falhou! </p>
<p style="text-align: center;"> Tenta diretamente na aplicação da trend.</p>

<p style="text-align: center;"> Obrigado, </p>
<p style="text-align: center;"> <a href="https://marcosmendes.net:8443/trend">{{ config('app.name') }} </a></p>
@endcomponent