@extends('layouts.app')

@section('content')
      <section class="section">
        @if (Session::has('message'))
               <div class="notification is-primary has-text-centered has-text-black is-size-4" id="notif">
                    <button class="delete" onclick="closeNotif()"></button>
                   {{Session::get('message')}}
               </div>
          @elseif (Session::has('error'))
               <div class="notification is-warning has-text-centered">
                   <button class="delete"></button>
                   {{Session::get('error')}}
               </div>
          @endif
        <div class="container has-text-centered">
          <h1 class="title">
            Marcações Trend
          </h1>  
          <br class="is-hidden-mobile">
          <div class="content has-text-centered">
            @if (Route::has('login'))
              @auth
                  <h3> Marcações para a semana <span class="tooltip is-tooltip-primary is-tooltip-active is-tooltip-bottom-mobile" 
                  data-tooltip="De {{$start}} a {{$end}}">W19{{$week}}</span>
                  e semanas seguintes do socio <span class="tooltip is-tooltip-primary is-tooltip-active is-tooltip-bottom-mobile" 
                  data-tooltip="{{Auth::user()->email}}">nº{{Auth::user()->nsocio}}</span>
                  </h3>
                  <br>
                </div>
              </div>
            <div class="content has-text-centered is-hidden-mobile">
                <form method="GET" action="{{ url('/change') }}">
                  <input class="switch is-rtl is-medium" id="emailSwitch" type="checkbox" 
                    name="wantsEmail" value="{{Auth::user()->wantsEmail}}" @if(Auth::user()->wantsEmail) checked @endif>
                  <label for="emailSwitch">Quero receber email de confirmação da marcação</label> 
                  <br>
                  <br>
                  <table class="table is-bordered">
                  <thead>
                  <tr><th></th><th class="has-text-centered">Segunda</th><th class="has-text-centered">Terça</th>
                    <th class="has-text-centered">Quarta</th><th class="has-text-centered">Quinta</th><th class="has-text-centered">Sexta</th><th class="has-text-centered">Sábado</th></tr>
                  </thead>
                  
                  <input type="hidden" name="email" value="{{Auth::user()->email}}">
                  <tbody>
                  <tr><th class="has-text-centered">7:15</th>
                  <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="segunda0715Switch" type="checkbox" 
                        name="segunda0715"  value="{{Auth::user()->segunda0715}}" @if(Auth::user()->segunda0715) checked @endif>
                      <label for="segunda0715Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="terca0715Switch" type="checkbox" 
                        name="terca0715"  value="{{Auth::user()->terca0715}}" @if(Auth::user()->terca0715) checked @endif>
                      <label for="terca0715Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="quarta0715Switch" type="checkbox" 
                        name="quarta0715"  value="{{Auth::user()->quarta0715}}" @if(Auth::user()->quarta0715) checked @endif>
                      <label for="quarta0715Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="quinta0715Switch" type="checkbox" 
                        name="quinta0715"  value="{{Auth::user()->quinta0715}}" @if(Auth::user()->quinta0715) checked @endif>
                      <label for="quinta0715Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="sexta0715Switch" type="checkbox" 
                        name="sexta0715"  value="{{Auth::user()->sexta0715}}" @if(Auth::user()->sexta0715) checked @endif>
                      <label for="sexta0715Switch"></label>
                    </div>
                    </td>
                  <td></td>
                  </tr>
                  <tr><th class="has-text-centered">10:00</th>
                  <td></td><td></td><td></td><td></td><td></td><td class="has-text-centered">
                    <div class="field">
                      <input class="switch is-medium" id="sabado1000Switch" type="checkbox" 
                        name="sabado1000"  value="{{Auth::user()->sabado1000}}" @if(Auth::user()->sabado1000) checked @endif>
                      <label for="sabado1000Switch"></label>
                    </div>
                    </td>
                  </tr>                      
                  <tr><th class="has-text-centered">11:00 (WL)</th>
                  <td></td><td></td><td></td><td></td><td></td><td class="has-text-centered">
                    <div class="field">
                      <input class="switch is-medium" id="sabado1100Switch" type="checkbox" 
                        name="sabado1100" value="{{Auth::user()->sabado1100}}" @if(Auth::user()->sabado1100) checked @endif>
                      <label for="sabado1100Switch"></label>
                    </div>
                    </td>
                  </tr>    
                  <tr><th class="has-text-centered">12:00</th>
                  <td></td><td></td><td></td><td></td><td></td><td class="has-text-centered">
                    <div class="field">
                      <input class="switch is-medium" id="sabado1200Switch" type="checkbox" 
                        name="sabado1200" value="{{Auth::user()->sabado1200}}" @if(Auth::user()->sabado1200) checked @endif>
                      <label for="sabado1200Switch"></label>
                    </div>
                    </td>
                  </tr>                   
                  <tr><th class="has-text-centered">19:00</th>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="switch is-medium" id="segunda1900Switch" type="checkbox" 
                        name="segunda1900" value="{{Auth::user()->segunda1900}}" @if(Auth::user()->segunda1900) checked @endif>
                      <label for="segunda1900Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="terca1900Switch" type="checkbox" 
                        name="terca1900" value="{{Auth::user()->terca1900}}" @if(Auth::user()->terca1900) checked @endif>
                      <label for="terca1900Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="quarta1900Switch" type="checkbox" 
                        name="quarta1900" value="{{Auth::user()->quarta1900}}" @if(Auth::user()->quarta1900) checked @endif>
                      <label for="quarta1900Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="quinta1900Switch" type="checkbox" 
                        name="quinta1900" value="{{Auth::user()->quinta1900}}" @if(Auth::user()->quinta1900) checked @endif>
                      <label for="quinta1900Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="sexta1900Switch" type="checkbox" 
                        name="sexta1900" value="{{Auth::user()->sexta1900}}" @if(Auth::user()->sexta1900) checked @endif>
                      <label for="sexta1900Switch"></label>
                    </div>
                    </td>
                    <td></td>
                  </tr>
                  <!-- <tr><th class="has-text-centered">19:30</th>
                  <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="segunda1930Switch" type="checkbox" 
                        name="segunda1930" value="{{Auth::user()->segunda1930}}" @if(Auth::user()->segunda1930) checked @endif>
                      <label for="segunda1930Switch"></label>
                    </div>
                    </td>
                    <td></td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="quarta1930Switch" type="checkbox" 
                        name="quarta1930" value="{{Auth::user()->quarta1930}}" @if(Auth::user()->quarta1930) checked @endif>
                      <label for="quarta1930Switch"></label>
                    </div>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>            
                  </tr> -->
                  <tr><th class="has-text-centered">20:00</th>
                  <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="segunda2000Switch" type="checkbox" 
                        name="segunda2000" value="{{Auth::user()->segunda2000}}" @if(Auth::user()->segunda2000) checked @endif>
                      <label for="segunda2000Switch"></label>
                    </div>  
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="terca2000Switch" type="checkbox" 
                        name="terca2000" value="{{Auth::user()->terca2000}}" @if(Auth::user()->terca2000) checked @endif>
                      <label for="terca2000Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="quarta2000Switch" type="checkbox" 
                        name="quarta2000" value="{{Auth::user()->quarta2000}}" @if(Auth::user()->quarta2000) checked @endif>
                      <label for="quarta2000Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="quinta2000Switch" type="checkbox" 
                        name="quinta2000" value="{{Auth::user()->quinta2000}}" @if(Auth::user()->quinta2000) checked @endif>
                      <label for="quinta2000Switch"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field ">
                      <input class="switch is-medium" id="sexta2000Switch" type="checkbox" 
                        name="sexta2000" value="{{Auth::user()->sexta2000}}" @if(Auth::user()->sexta2000) checked @endif>
                      <label for="sexta2000Switch"></label>
                    </div>
                    </td>
                    <td></td>
                  </tr>
                  </tbody>
                  </table>
                  <br>
                  <button class="button is-primary is-rounded">
                    <span class="icon">
                      <i class="fas fa-exchange-alt"></i>
                    </span>
                    <span>Guardar</span> 
                  </button>
                  </form>
                  </section>
                </div>  
                <div class="content has-text-centered is-hidden-tablet" style="margin-left: 5px;margin-right: 5px;">
                  <form method="GET" action="{{ url('/change') }}">
                  <input class="is-checkradio is-rtl is-circle is-small" id="emailCheckbox" type="checkbox" 
                      name="wantsEmail" value="{{Auth::user()->wantsEmail}}" @if(Auth::user()->wantsEmail) checked @endif>
                    <label for="emailCheckbox">Quero receber email de confirmação da marcação</label> 
                  <br>
                  <br>
                  <table class="table is-bordered">
                  <thead>
                  <tr><th></th><th class="has-text-centered">2ª</th><th class="has-text-centered">3ª</th><th class="has-text-centered">4ª</th>
                    <th class="has-text-centered">5ª</th><th class="has-text-centered">6ª</th><th class="has-text-centered">Sab</th></tr>
                  </thead>
                  
                  <input type="hidden" name="email" value="{{Auth::user()->email}}">
                  <tbody>
                  <tr><th class="has-text-centered">7:15</th>
                  <td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="segunda0715Checkbox" type="checkbox" 
                        name="segunda0715"  value="{{Auth::user()->segunda0715}}" @if(Auth::user()->segunda0715) checked @endif>
                      <label for="segunda0715Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="terca0715Checkbox" type="checkbox" 
                        name="terca0715"  value="{{Auth::user()->terca0715}}" @if(Auth::user()->terca0715) checked @endif>
                      <label for="terca0715Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="quarta0715Checkbox" type="checkbox" 
                        name="quarta0715"  value="{{Auth::user()->quarta0715}}" @if(Auth::user()->quarta0715) checked @endif>
                      <label for="quarta0715Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="quinta0715Checkbox" type="checkbox" 
                        name="quinta0715"  value="{{Auth::user()->quinta0715}}" @if(Auth::user()->quinta0715) checked @endif>
                      <label for="quinta0715Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="sexta0715Checkbox" type="checkbox" 
                        name="sexta0715"  value="{{Auth::user()->sexta0715}}" @if(Auth::user()->sexta0715) checked @endif>
                      <label for="sexta0715Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div> 
                    </td>
                  <td></td>
                  </tr>  
                  <tr><th class="has-text-centered">10:00</th>
                  <td></td><td></td><td></td><td></td><td></td><td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="sabado1000Checkbox" type="checkbox" 
                        name="sabado1000"  value="{{Auth::user()->sabado1000}}" @if(Auth::user()->sabado1000) checked @endif>
                      <label for="sabado1000Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    
                    </td>
                  </tr>                      
                  <tr><th class="has-text-centered">11:00 (WL)</th>
                  <td></td><td></td><td></td><td></td><td></td><td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="sabado1100Checkbox" type="checkbox" 
                        name="sabado1100" value="{{Auth::user()->sabado1100}}" @if(Auth::user()->sabado1100) checked @endif>
                      <label for="sabado1100Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                  </tr>    
                  <tr><th class="has-text-centered">12:00</th>
                  <td></td><td></td><td></td><td></td><td></td><td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="sabado1200Checkbox" type="checkbox" 
                        name="sabado1200" value="{{Auth::user()->sabado1200}}" @if(Auth::user()->sabado1200) checked @endif>
                      <label for="sabado1200Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                  </tr>                    
                  <tr><th class="has-text-centered">19:00</th>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="segunda1900Checkbox" type="checkbox" 
                        name="segunda1900" value="{{Auth::user()->segunda1900}}" @if(Auth::user()->segunda1900) checked @endif>
                      <label for="segunda1900Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="terca1900Checkbox" type="checkbox" 
                        name="terca1900" value="{{Auth::user()->terca1900}}" @if(Auth::user()->terca1900) checked @endif>
                      <label for="terca1900Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="quarta1900Checkbox" type="checkbox" 
                        name="quarta1900" value="{{Auth::user()->quarta1900}}" @if(Auth::user()->quarta1900) checked @endif>
                      <label for="quarta1900Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="quinta1900Checkbox" type="checkbox" 
                        name="quinta1900" value="{{Auth::user()->quinta1900}}" @if(Auth::user()->quinta1900) checked @endif>
                      <label for="quinta1900Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="sexta1900Checkbox" type="checkbox" 
                        name="sexta1900" value="{{Auth::user()->sexta1900}}" @if(Auth::user()->sexta1900) checked @endif>
                      <label for="sexta1900Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td></td>
                  </tr>
                  <!-- <tr><th class="has-text-centered">19:30</th>
                  <td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="segunda1930Checkbox" type="checkbox" 
                        name="segunda1930" value="{{Auth::user()->segunda1930}}" @if(Auth::user()->segunda1930) checked @endif>
                      <label for="segunda1930Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td></td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="quarta1930Checkbox" type="checkbox" 
                        name="quarta1930" value="{{Auth::user()->quarta1930}}" @if(Auth::user()->quarta1930) checked @endif>
                      <label for="quarta1930Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>            
                  </tr> -->
                  <tr><th class="has-text-centered">20:00</th>
                  <td class="has-text-centered">
                  <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="segunda2000Checkbox" type="checkbox" 
                        name="segunda2000" value="{{Auth::user()->segunda2000}}" @if(Auth::user()->segunda2000) checked @endif>
                      <label for="segunda2000Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="terca2000Checkbox" type="checkbox" 
                        name="terca2000" value="{{Auth::user()->terca2000}}" @if(Auth::user()->terca2000) checked @endif>
                      <label for="terca2000Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="quarta2000Checkbox" type="checkbox" 
                        name="quarta2000" value="{{Auth::user()->quarta2000}}" @if(Auth::user()->quarta2000) checked @endif>
                      <label for="quarta2000Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="quinta2000Checkbox" type="checkbox" 
                        name="quinta2000" value="{{Auth::user()->quinta2000}}" @if(Auth::user()->quinta2000) checked @endif>
                      <label for="quinta2000Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td class="has-text-centered">
                    <div class="field">
                      <input class="is-checkradio is-rtl is-circle is-small" id="sexta2000Checkbox" type="checkbox" 
                        name="sexta2000" value="{{Auth::user()->sexta2000}}" @if(Auth::user()->sexta2000) checked @endif>
                      <label for="sexta2000Checkbox" style="margin-left: 0px; padding-right: 20px;"></label>
                    </div>
                    </td>
                    <td></td>
                  </tr>
                  </tbody>
                  </table>
                  <button class="button is-primary is-rounded">
                    <span class="icon">
                      <i class="fas fa-exchange-alt"></i>
                    </span>
                    <span>Guardar</span> 
                  </button>
                  </form>
                </div>
              @else
                  <br>
                  <h2>Nenhum utilizador com login efetuado.</h2>
                  <br>
                  <h3>Escolha uma opção</h3>
                  <a class="button is-primary is-rounded" href="{{ route('login') }}">
                    <span class="icon">
                      <i class="fas fa-sign-in-alt"></i>
                    </span>
                    <span>Fazer login</span> 
                  </a>
                  <br>
                  <br>
                  <a class="button is-primary is-rounded" href="{{ route('register') }}">
                    <span class="icon">
                    <i class="fas fa-plus"></i>
                    </span>
                    <span>Registrar</span> 
                  </a>
                </div>
              </div>
            </section>
              @endauth
            @endif
            <section class="section">
              <br>
              <div class="container has-text-centered is-hidden-mobile"> 
                <h1 class="title">Ultimas Marcações da APP</h1>
                <p>{!! nl2br(e($log)) !!}</p>
              </div>
              <div class="container has-text-centered is-hidden-tablet"> 
                <h1 class="title">Ultimas Marcações</h1>
                <p style="font-size: 0.8rem">{!! nl2br(e($logM)) !!}</p>
              </div>
            </section>
            
@endsection