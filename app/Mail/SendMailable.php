<?php

namespace App\Mail;

use app\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    public $user;
    public $data;
    public $linkGoogle;

    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $data, $linkGoogle)
    {
        $this->user = $user;
        $this->data = $data;
        $this->linkGoogle = $linkGoogle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('emails.user');
        return $this->markdown('emails.user')->subject('Nova marcação Trend')->attach('aulaCalendario.ics');
    }
}
