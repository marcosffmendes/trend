<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:4|confirmed',
            'passwordApp' => 'required|string|min:4',
            'nsocio' => 'required|min:2|numeric',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'passwordApp' => strrev($data['passwordApp']),
            'nsocio' => $data['nsocio'],
            'nickname' => NULL,
            'segunda0715' => false,
            'segunda1900' => false,
            'segunda1930' => false,
            'segunda2000' => false,
            'terca0715' => false,
            'terca1900' => false,
            'terca2000' => false,
            'quarta0715' => false,
            'quarta1900' => false,
            'quarta1930' => false,
            'quarta2000' => false,
            'quinta0715' => false,
            'quinta1900' => false,
            'quinta2000' => false,
            'sexta0715' => false,
            'sexta1900' => false,
            'sexta2000' => false,
            'sabado1000' => false,
            'sabado1100' => false,
            'sabado1200' => false,
            'wantsEmail' => false,
        ]);
    }
}
