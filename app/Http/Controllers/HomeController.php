<?php

namespace App\Http\Controllers;

use Session;
use Redirect;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function update(Request $request)
    {
    
        //dd($request->input('segunda1900'));
        $formData = $request->all();
        $user = User::where('email', $formData['email'])->first();
        
        if(!isset($formData['segunda0715'])){$user->segunda0715 = 0;}else{$user->segunda0715 = 1;}
        if(!isset($formData['segunda1900'])){$user->segunda1900 = 0;}else{$user->segunda1900 = 1;}
        if(!isset($formData['segunda1930'])){$user->segunda1930 = 0;}else{$user->segunda1930 = 1;}
        if(!isset($formData['segunda2000'])){$user->segunda2000 = 0;}else{$user->segunda2000 = 1;}
        if(!isset($formData['terca0715'])){$user->terca0715 = 0;}else{$user->terca0715 = 1;}
        if(!isset($formData['terca1900'])){$user->terca1900 = 0;}else{$user->terca1900 = 1;}
        if(!isset($formData['terca2000'])){$user->terca2000 = 0;}else{$user->terca2000 = 1;}
        if(!isset($formData['quarta0715'])){$user->quarta0715 = 0;}else{$user->quarta0715 = 1;}
        if(!isset($formData['quarta1900'])){$user->quarta1900 = 0;}else{$user->quarta1900 = 1;}
        if(!isset($formData['quarta1930'])){$user->quarta1930 = 0;}else{$user->quarta1930 = 1;}
        if(!isset($formData['quarta2000'])){$user->quarta2000 = 0;}else{$user->quarta2000 = 1;}
        if(!isset($formData['quinta0715'])){$user->quinta0715 = 0;}else{$user->quinta0715 = 1;}
        if(!isset($formData['quinta1900'])){$user->quinta1900 = 0;}else{$user->quinta1900 = 1;}
        if(!isset($formData['quinta2000'])){$user->quinta2000 = 0;}else{$user->quinta2000 = 1;}
        if(!isset($formData['sexta0715'])){$user->sexta0715 = 0;}else{$user->sexta0715 = 1;}
        if(!isset($formData['sexta1900'])){$user->sexta1900 = 0;}else{$user->sexta1900 = 1;}
        if(!isset($formData['sexta2000'])){$user->sexta2000 = 0;}else{$user->sexta2000 = 1;}
        if(!isset($formData['sabado1000'])){$user->sabado1000 = 0;}else{$user->sabado1000 = 1;}
        if(!isset($formData['sabado1100'])){$user->sabado1100 = 0;}else{$user->sabado1100 = 1;}
        if(!isset($formData['sabado1200'])){$user->sabado1200 = 0;}else{$user->sabado1200 = 1;}
        if(!isset($formData['wantsEmail'])){$user->wantsEmail = 0;}else{$user->wantsEmail = 1;}
            
        $user->save();
    
        Session::flash('message', "Alterações efetuadas com sucesso!");
        return Redirect::back();
    }
}
