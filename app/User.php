<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'passwordApp', 'nsocio', 'segunda0715', 'segunda1900', 'segunda1930', 'segunda2000', 'terca0715', 'terca1900', 'terca2000', 
        'quarta0715', 'quarta1900', 'quarta1930', 'quarta2000', 'quinta0715', 'quinta1900', 'quinta2000', 'sexta0715', 'sexta1900', 'sexta2000', 'sabado1000', 'sabado1100', 'wantsEmail',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
}
