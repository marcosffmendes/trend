<?php

namespace App\Console;

use Log;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Mail\SendMailable;
use App\Mail\SendMailableNOK;
use Spatie\CalendarLinks\Link;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->call(function () {
            ####### Trend crossfit ######
            //Log::info('Sem marcações ainda...');
            /* $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            Log::info($todayPlusTwoUnix.' '.$week.' '.$year);  */
            
            /* $user = User::first();
            $day = Carbon::now()->addDay(2)->day;
            $data = "segunda feira (dia ".$day.") às 7:15";
            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'), new \DateTimeZone('WEST'));
            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour(2)->format('Y-m-d H:i'), new \DateTimeZone('WEST'));
            $link = Link::create('Treino Trend', $from, $to)
                ->description('Treino Trend')
                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
            $linkGoogle = $link->google();
            $linkApple = $link->ics();
            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
            $filename = "aulaCalendario.ics";
            file_put_contents($filename, $linkApple);
            Mail::to('marcosfilipemendes@gmail.com')->send(new SendMailable($user, $data, $linkGoogle));
 */
        //})->everyFifteenMinutes();
        //})->everyMinute();

        $schedule->call(function () {
            ####### Trend crossfit Marcações para segunda às 07:15 ######
            Log::info('Marcações para segunda às 07:15 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->segunda0715 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '109', 'semana' => $week, 'ano' => $year, 'id' => '1', 'data' => $todayPlusTwoUnix]]);
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "segunda feira (dia ".$day.") às 7:15";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para segunda às 07:15 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "segunda feira (dia ".$day.") às 7:15";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }    
                    }
                }
            }
            Log::info('Marcações para segunda às 07:15:');
        })->weeklyOn(6, '7:15');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para segunda às 19:00 ######
            Log::info('Marcações para segunda às 19:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->segunda1900 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '143', 'semana' => $week, 'ano' => $year, 'id' => '5', 'data' => $todayPlusTwoUnix]]);
        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "segunda feira (dia ".$day.") às 19:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para segunda às 19:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "segunda feira (dia ".$day.") às 19:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }  
                    }
                }
            }
            Log::info('Marcações para segunda às 19:00:');
        })->weeklyOn(6, '19:00');

        /* $schedule->call(function () {
            ####### Trend crossfit Marcações para segunda às 19:30 ######
            Log::info('Marcações para segunda às 19:30 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->segunda1930 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '136', 'semana' => $week, 'ano' => $year, 'id' => '8', 'data' => $todayPlusTwoUnix]]);
        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "segunda feira (dia ".$day.") às 19:30";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para segunda às 19:30 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "segunda feira (dia ".$day.") às 19:30";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            } 
                    }
                }
            }
            Log::info('Marcações para segunda às 19:30:');
        })->weeklyOn(6, '19:30'); */

        $schedule->call(function () {
            ####### Trend crossfit Marcações para segunda às 20:00 ######
            Log::info('Marcações para segunda às 20:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->segunda2000 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '144', 'semana' => $week, 'ano' => $year, 'id' => '6', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "segunda feira (dia ".$day.") às 20:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para segunda às 20:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "segunda feira (dia ".$day.") às 20:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para segunda às 20:00:');
        })->weeklyOn(6, '20:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para terça às 07:15 ######
            Log::info('Marcações para terça às 07:15 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->terca0715 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '117', 'semana' => $week, 'ano' => $year, 'id' => '1', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "terça feira (dia ".$day.") às 7:15";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para terça às 07:15 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "terça feira (dia ".$day.") às 7:15";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para terça às 07:15:');
        })->weeklyOn(7, '7:15');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para terça às 19:00 ######
            Log::info('Marcações para terça às 19:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->terca1900 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '123', 'semana' => $week, 'ano' => $year, 'id' => '5', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "terça feira (dia ".$day.") às 19:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para terça às 19:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "terça feira (dia ".$day.") às 19:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para terça às 19:00:');
        })->weeklyOn(7, '19:00');
        
        $schedule->call(function () {
            ####### Trend crossfit Marcações para terça às 20:00 ######
            Log::info('Marcações para terça às 20:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->terca2000 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '124', 'semana' => $week, 'ano' => $year, 'id' => '6', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "terça feira (dia ".$day.") às 20:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para terça às 20:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "terça feira (dia ".$day.") às 20:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para terça às 20:00:');
        })->weeklyOn(7, '20:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para quarta às 07:15 ######
            Log::info('Marcações para quarta às 07:15 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->quarta0715 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '78', 'semana' => $week, 'ano' => $year, 'id' => '1', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "quarta feira (dia ".$day.") às 7:15";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para quarta às 07:15 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "quarta feira (dia ".$day.") às 7:15";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para quarta às 07:15:');
        })->weeklyOn(1, '7:15');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para quarta às 19:00 ######
            Log::info('Marcações para quarta às 19:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->quarta1900 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '140', 'semana' => $week, 'ano' => $year, 'id' => '5', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "quarta feira (dia ".$day.") às 19:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para quarta às 19:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "quarta feira (dia ".$day.") às 19:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para quarta às 19:00:');
        })->weeklyOn(1, '19:00');

        /* $schedule->call(function () {
            ####### Trend crossfit Marcações para quarta às 19:30 ######
            Log::info('Marcações para quarta às 19:30 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();

            foreach($users as $user){
                if($user->quarta1930 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '142', 'semana' => $week, 'ano' => $year, 'id' => '7', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "quarta feira (dia ".$day.") às 19:30";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para quarta às 19:30 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "quarta feira (dia ".$day.") às 19:30";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para quarta às 19:30:');
        })->weeklyOn(1, '19:30'); */

        $schedule->call(function () {
            ####### Trend crossfit Marcações para quarta às 20:00 ######
            Log::info('Marcações para quarta às 20:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();

            foreach($users as $user){
                if($user->quarta2000 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '141', 'semana' => $week, 'ano' => $year, 'id' => '6', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "quarta feira (dia ".$day.") às 20:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para quarta às 20:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "quarta feira (dia ".$day.") às 20:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para quarta às 20:00:');
        })->weeklyOn(1, '20:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para quinta às 07:15 ######
            Log::info('Marcações para quinta às 07:15 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->quinta0715 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '58', 'semana' => $week, 'ano' => $year, 'id' => '1', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "quinta feira (dia ".$day.") às 7:15";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para quinta às 07:15 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "quinta feira (dia ".$day.") às 7:15";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para quinta às 07:15:');
        })->weeklyOn(2, '7:15');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para quinta às 19:00 ######
            Log::info('Marcações para quinta às 19:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->quinta1900 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '147', 'semana' => $week, 'ano' => $year, 'id' => '5', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "quinta feira (dia ".$day.") às 19:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para quinta às 19:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "quinta feira (dia ".$day.") às 19:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para quinta às 19:00:');
        })->weeklyOn(2, '19:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para quinta às 20:00 ######
            Log::info('Marcações para quinta às 20:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->quinta2000 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '148', 'semana' => $week, 'ano' => $year, 'id' => '6', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "quinta feira (dia ".$day.") às 20:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para quinta às 20:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "quinta feira (dia ".$day.") às 20:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para quinta às 20:00:');
        })->weeklyOn(2, '20:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para sexta às 07:15 ######
            Log::info('Marcações para sexta às 07:15 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->sexta0715 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '46', 'semana' => $week, 'ano' => $year, 'id' => '1', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "sexta feira (dia ".$day.") às 7:15";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sexta às 07:15 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "sexta feira (dia ".$day.") às 7:15";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para sexta às 07:15:');
        })->weeklyOn(3, '7:15');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para sexta às 19:00 ######
            Log::info('Marcações para sexta às 19:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->sexta1900 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '105', 'semana' => $week, 'ano' => $year, 'id' => '5', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "sexta feira (dia ".$day.") às 19:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sexta às 19:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "sexta feira (dia ".$day.") às 19:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para sexta às 19:00:');
        })->weeklyOn(3, '19:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para sexta às 20:00 ######
            Log::info('Marcações para sexta às 20:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->sexta2000 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '106', 'semana' => $week, 'ano' => $year, 'id' => '6', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "sexta feira (dia ".$day.") às 20:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sexta às 20:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "sexta feira (dia ".$day.") às 20:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para sexta às 20:00:');
        })->weeklyOn(3, '20:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para sabado às 10:00 ######
            Log::info('Marcações para sabado às 10:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->sabado1000 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '102', 'semana' => $week, 'ano' => $year, 'id' => '1', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "sábado (dia ".$day.") às 10:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sabado às 10:00 efetuada com sucesso!');
                        //Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sabado às 10:00 (WL) efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "sábado (dia ".$day.") às 10:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para sabado às 10:00:');
        })->weeklyOn(4, '10:00');

        $schedule->call(function () {
            ####### Trend crossfit Marcações para sabado às 11:00 ######
            Log::info('Marcações para sabado às 11:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->sabado1100 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '150', 'semana' => $week, 'ano' => $year, 'id' => '2', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "sábado (dia ".$day.") às 11:00 (trend wod)";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->addMinute(30)->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sabado às 11:00 (WL) efetuada com sucesso!');
                        //Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sabado às 11:00 (trend wod) efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                //$data = "sábado (dia ".$day.") às 11:00 (trend wod)";
                                $data = "sábado (dia ".$day.") às 11:00 (WL)";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para sabado às 11:00:');
        })->weeklyOn(4, '11:00');
        
        $schedule->call(function () {
            ####### Trend crossfit Marcações para sabado às 12:00 ######
            Log::info('Marcações para sabado às 12:00 efetuadas!');
            $now = Carbon::now();
            $todayPlusTwoUnix = mktime(0, 0, 0, $now->month, $now->addDays(2)->day, $now->year);
            $week = $now->weekOfYear;
            $year = $now->year;
            $users = User::all();
            foreach($users as $user){
                if($user->sabado1200 == 1){
                    try {
                        $client = new Client(['base_uri' => 'https://fitgestpro.pt/mytrendcrossfit/app/']);
        
                        $response = $client->request('POST', 'register_schedule.php', ['form_params' => ['username' => $user->email, 'password' => strrev($user->passwordApp), 
                        'nsocio' => $user->nsocio, 'horario' => '152', 'semana' => $week, 'ano' => $year, 'id' => '3', 'data' => $todayPlusTwoUnix]]);
        
                        
                        //dd($response);
                        
                        if($user->wantsEmail == 1){
                            $day = Carbon::now()->addDay(2)->day;
                            $data = "sábado (dia ".$day.") às 12:00";
                            $from = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
                            $to = \DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
                            $link = Link::create('Treino Trend', $from, $to)
                                ->description('Treino Trend')
                                ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
                            $linkGoogle = $link->google();
                            $linkApple = $link->ics();
                            $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
                            $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
                            $filename = "aulaCalendario.ics";
                            file_put_contents($filename, $linkApple);
                            Mail::to($user->email)->send(new SendMailable($user, $data, $linkGoogle));
                            Log::info('Email enviado para '.$user->email. '!');
                        }
                        Log::info('Marcação do sócio '.$user->nsocio.' ('.$user->nickname.') para sabado às 12:00 efetuada com sucesso!');

                    }catch (\GuzzleHttp\Exception\GuzzleException $e) {
                            //dd('exception caught:'. $e);
                            Log::info('Erro! Mais informação: '. $e);
                            Log::info('Aula NÂO marcada para sócio '. $user->nsocio.' ('.$user->nickname.')');
                            if($user->wantsEmail == 1){
                                $day = Carbon::now()->addDay(2)->day;
                                $data = "sábado (dia ".$day.") às 13:00";
                                Mail::to($user->email)->send(new SendMailableNOK($user, $data));
                            }
                    }
                }
            }
            Log::info('Marcações para sabado às 12:00:');
        })->weeklyOn(4, '12:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
