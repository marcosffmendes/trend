<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('passwordApp');
            $table->integer('nsocio');
            $table->boolean('segunda1900');
            $table->boolean('segunda1930');
            $table->boolean('segunda2000');
            $table->boolean('terca1900');
            $table->boolean('terca2000');
            $table->boolean('quarta1900');
            $table->boolean('quarta1930');
            $table->boolean('quarta2000');
            $table->boolean('quinta1900');
            $table->boolean('quinta2000');
            $table->boolean('sexta1900');
            $table->boolean('sexta2000');
            $table->boolean('sabado1000');
            $table->boolean('sabado1100');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
