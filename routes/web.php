<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Mail\SendMailable;
use App\Mail\SendMailableNOK;
use Spatie\CalendarLinks\Link;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;

Route::get('/', function () { 
    $week = Carbon::now()->weekOfYear;
    if(Carbon::now()->dayOfWeek >= 5){
        $week = $week+1;
    }
    $year = Carbon::now()->year;
    $date = Carbon::now();
    $date->setISODate($year,$week);
    $start = $date->startOfWeek()->format('d/m');
    $end = $date->endOfWeek()->format('d/m');

    $log = storage_path('logs/laravel.log');
    $log = escapeshellarg($log); // for the security concious (should be everyone!)
    $linesSplit = `tail -n 12 $log | tac`;
    $lines = str_replace("production.INFO: ", " ", $linesSplit);
    $linesM = str_replace("]", "]\n", $lines);
    //echo ($lines);

    return view('welcome')->with('week', $week)->with('start', $start)->with('end', $end)->with('log', $lines)->with('logM', $linesM);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/change', 'HomeController@update')->name('change');

Route::get('/test', function () { 
    //$user = User::find(2);
    $user = User::first();

    $now = Carbon::now();

    //$day = Carbon::now()->addDay(2)->day;
    //$data = "segunda feira (dia ".$day.") às 7:15";

    //Mail::to('marcosmendes@hotmail.com')->send(new SendMailableNOK($user, $data));
    dd($now);
    $day = Carbon::now()->addDay(2)->day;
    $data = "segunda feira (dia ".$day.") às 7:15";
    $from = DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->format('Y-m-d H:i'));
    $to = DateTime::createFromFormat('Y-m-d H:i', Carbon::now()->addDay(2)->addHour()->format('Y-m-d H:i'));
    $link = Link::create('Treino Trend', $from, $to)
        ->description('Treino Trend')
        ->address('Rua Bulhão Pato 3E, 1700-081 Lisboa');
    $linkGoogle = $link->google();
    $linkApple = $link->ics();
    $linkApple = str_replace('%0d%0a', PHP_EOL, $linkApple);
    $linkApple = str_replace('data:text/calendar;charset=utf8,', '', $linkApple);
    $filename = "aulaCalendario.ics";
    file_put_contents($filename, $linkApple);
    //echo($linkApple);
    //dd('exit');
    Mail::to('marcosmendes@hotmail.com')->send(new SendMailable($user, $data, $linkGoogle));
    return 'Email was sent';
});
